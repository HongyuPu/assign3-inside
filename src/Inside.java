/**
 * Assignment 3 for CS 1410
 * This program determines if points are contained within circles or rectangles.
 *
 * @author James Dean Mathias
 */
public class Inside {
    /**
     * This is the primary driver code to test the "inside" capabilities of the
     * various functions.
     */
    public static void main(String[] args) {
        double[] ptX = { 1, 2, 3, 4 };
        double[] ptY = { 1, 2, 3, 4 };
        double[] circleX = { 0, 5 };
        double[] circleY = { 0, 5 };
        double[] circleRadius = { 3, 3 };
        double[] rectLeft = { -2.5, -2.5 };
        double[] rectTop = { 2.5, 5.0 };
        double[] rectWidth = { 6.0, 5.0 };
        double[] rectHeight = { 5.0, 2.5 };
        System.out.println("--- Report of Points and Circles ---" + "\n");
        for (int i = 0; i < circleX.length; i++) { //prints out the circle statements
            for (int j = 0; j < ptX.length; j++) {
                reportPoint(ptX[j], ptY[j]);
                if (isPointInsideCircle(ptX[j], ptY[j], circleX[i], circleY[i], circleRadius[i])) {
                    System.out.print(" is inside ");
                }
                else {
                    System.out.print(" is outside ");
                }
                reportCircle(circleX[i], circleY[i], circleRadius[i]);
                System.out.println();
            }
        }
        System.out.println();

        System.out.println("--- Report of Points and Rectangles ---" + "\n");
        for (int i = 0; i < rectLeft.length; i++) { //prints out the rectangle statements
            for (int j = 0; j < ptX.length; j++) {
                reportPoint(ptX[j], ptY[j]);
                if (isPointInsideRectangle(ptY[j], ptX[j], rectLeft[i], rectTop[i], rectHeight[i], rectWidth[i])) {
                    System.out.print(" is inside ");
                }
                else {
                    System.out.print(" is outside ");
                }
                reportRectangle(rectLeft[i], rectTop[i], rectWidth[i], rectHeight[i]);
                System.out.println();
            }
        }
    }

    public static void reportPoint(double x, double y) { //prints out a report of the coordinates

        System.out.print("Point" + "(" + x + ", " + y + ")");
    }

    public static void reportCircle(double x, double y, double r) { //prints out a report of the circles
        System.out.print("Circle(" + x + ", " + y + ") " + "Radius: " + r);
    }

    public static void reportRectangle(double left, double top, double width, double height) { //prints out a report of the rectangles
        double right = left + width;
        double bottom = top - height;
        System.out.print("Rectangle" + "(" + left + ", " + top + ", " + right + ", " + bottom + ")");
    }

    public static boolean isPointInsideCircle(double ptX, double ptY, double circleX, double circleY, double circleRadius) { //determines if point is inside circle
        double distance = Math.sqrt(Math.pow((circleX - ptX), 2) + Math.pow((circleY - ptY), 2));
        if (distance <= circleRadius) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isPointInsideRectangle(double ptX, double ptY, double rLeft, double rTop, double rWidth, double rHeight) { //determines if a point is inside rectangle
        if (ptX >= rLeft && ptX <= rLeft + rWidth && ptY <= rTop && ptY >= rTop - rHeight) {
            return true;
        }
        else {
            return false;
        }
    }
}
